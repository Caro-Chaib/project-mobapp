package com.example.projetl3.webservice;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class JSONResponseHandlerIds {

    ArrayList<String> ids;
    private static final String TAG = JSONResponseHandlerDevice.class.getSimpleName();

    public JSONResponseHandlerIds(ArrayList<String> ids) {
        this.ids = ids;
    }

    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            Log.d(TAG, "Call readDevices : ");
            readDevices(reader);
        } finally {
            reader.close();
        }
    }

    public void readDevices(JsonReader reader) throws IOException {
        reader.beginArray();
        while (reader.hasNext()) {
            ids.add(reader.nextString());
        }
        reader.endArray();
    }
}
