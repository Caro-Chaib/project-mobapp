package com.example.projetl3.webservice;

import android.util.JsonReader;
import android.util.JsonToken;
import android.util.Log;

import com.example.projetl3.data.Device;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JSONResponseHandlerDevice {

    private static final String TAG = JSONResponseHandlerDevice.class.getSimpleName();

    private Device device;


    public JSONResponseHandlerDevice(Device device) {
        this.device = device;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */
    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            Log.d(TAG, "Call readDevices : " + device.getId());
            readDevices(reader);
        } finally {
            reader.close();
        }
    }

    public void readDevices(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("technicalDetails")) {
                reader.beginArray();
                String toAdd = reader.nextString();
                JsonToken peek = reader.peek();
                while (toAdd != null && peek != JsonToken.END_ARRAY) {
                    device.addTechnicalDetails(toAdd);
                    toAdd = reader.nextString();
                    peek = reader.peek();
                }
                reader.endArray();
            } else if (name.equals("name")) {
                device.setName(reader.nextString());
            } else if (name.equals("brand")) {
                device.setBrand(reader.nextString());
                Log.d(TAG, "Brand : " + device.getBrand());
            } else if (name.equals("name")) {
                device.setName(reader.nextString());
            } else if (name.equals("year")) {
                device.setYear(reader.nextInt());
            } else if (name.equals("technicalDetails")) {
                reader.beginArray();
                String toAdd = reader.nextString();
                JsonToken peek = reader.peek();
                while (toAdd != null && peek != JsonToken.END_ARRAY) {
                    device.addTechnicalDetails(toAdd);
                    toAdd = reader.nextString();
                    peek = reader.peek();
                }
                reader.endArray();
            } else if (name.equals("description")) {
                device.setDescription(reader.nextString());
            } else if (name.equals("timeFrame")) {
                reader.beginArray();
                device.setFrameBegin(reader.nextInt());
                JsonToken peek = reader.peek();
                while (peek != JsonToken.END_ARRAY) {
                    device.setFrameEnd(reader.nextInt());
                    peek = reader.peek();
                }
                reader.endArray();
            } else if (name.equals("categories")) {
                reader.beginArray();
                String toAdd = reader.nextString();
                JsonToken peek = reader.peek();
                while (toAdd != null && peek != JsonToken.END_ARRAY) {
                    device.addCategories(toAdd);
                    toAdd = reader.nextString();
                    peek = reader.peek();
                }
                reader.endArray();
            } else if (name.equals("working")) {
                device.setAsWorking(reader.nextBoolean());
                    /*else if (name.equals("intAwayScore")) {
                    JsonToken peek = reader.peek();
                    if (peek == JsonToken.NULL) {
                        match.setAwayScore(0);
                        reader.skipValue();
                    }
                    else match.setAwayScore(reader.nextInt());*/
            } else {
                reader.skipValue();
            }
            //readArrayDevices(reader);
        }
        reader.endObject();
    }

    private void readArrayDevices(JsonReader reader) throws  IOException {
        reader.beginArray();
        String name = reader.nextName();
        if (name.equals("brand")) {
            device.setBrand(reader.nextString());
            Log.d(TAG, "Brand : "+device.getBrand());
        } else if (name.equals("year")) {
            device.setYear(reader.nextInt());
        } else if (name.equals("technicalDetails")) {
            String toAdd = reader.nextString();
            while(toAdd != null) {
                device.addTechnicalDetails(toAdd);
                toAdd = reader.nextString();
            }
        } else if (name.equals("description")) {
            device.setDescription(reader.nextString());
        }
        else if (name.equals("timeFrame")) {
            int toAdd = reader.nextInt();
            device.setFrameBegin(toAdd);
            toAdd = reader.nextInt();
            while(toAdd != 0) {
                device.setFrameEnd(toAdd);
                toAdd = reader.nextInt();
            }
        } else if (name.equals("categories")) {
            String toAdd = reader.nextString();
            while(toAdd != null) {
                device.addCategories(toAdd);
                toAdd = reader.nextString();
            }
        } else if (name.equals("working")) {
            device.setAsWorking(reader.nextBoolean());
                        /*else if (name.equals("intAwayScore")) {
                        JsonToken peek = reader.peek();
                        if (peek == JsonToken.NULL) {
                            match.setAwayScore(0);
                            reader.skipValue();
                        }
                        else match.setAwayScore(reader.nextInt());*/
        } else {
            reader.skipValue();
        }
    }

    private void readArrayMatchs(JsonReader reader) throws IOException {
        Log.d(TAG, "start reading");
        //reader.beginArray();
        int nb = 0; // only consider the first element of the array
            reader.beginArray();
            while (reader.hasNext()) {
                //String name = reader.nextName();
                String name = reader.nextString();
                //if (nb>=0) {
                    if (name.equals("brand")) {
                        device.setBrand(reader.nextString());
                        Log.d(TAG, "Brand : "+device.getBrand());
                    } else if (name.equals("year")) {
                        device.setYear(reader.nextInt());
                    } else if (name.equals("technicalDetails")) {
                        String toAdd = reader.nextString();
                        while(toAdd != null) {
                            device.addTechnicalDetails(toAdd);
                            toAdd = reader.nextString();
                        }
                    } else if (name.equals("description")) {
                        device.setDescription(reader.nextString());
                    }
                    else if (name.equals("timeFrame")) {
                        int toAdd = reader.nextInt();
                        device.setFrameBegin(toAdd);
                        toAdd = reader.nextInt();
                        while(toAdd != 0) {
                            device.setFrameEnd(toAdd);
                            toAdd = reader.nextInt();
                        }
                    } else if (name.equals("categories")) {
                        String toAdd = reader.nextString();
                        while(toAdd != null) {
                            device.addCategories(toAdd);
                            toAdd = reader.nextString();
                        }
                    } else if (name.equals("working")) {
                        device.setAsWorking(reader.nextBoolean());
                        /*else if (name.equals("intAwayScore")) {
                        JsonToken peek = reader.peek();
                        if (peek == JsonToken.NULL) {
                            match.setAwayScore(0);
                            reader.skipValue();
                        }
                        else match.setAwayScore(reader.nextInt());*/
                    } else {
                        reader.skipValue();
                    }
                /*}  else {
                    reader.skipValue();
                }*/
            }
            reader.endArray();
            nb++;
        /*}
        reader.endArray();*/
    }

}
