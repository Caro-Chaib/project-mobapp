package com.example.projetl3.webservice;

import android.net.Uri;
import android.util.Log;

import java.net.MalformedURLException;
import java.net.URL;

public class WebServiceUrl {

    // constant string used as s parameter for lookuptable
    private static final String CUR_YEAR = "1920";

    private static final String HOST = "demo-lia.univ-avignon.fr";
    private static final String PATH_1 = "cerimuseum";

    private static Uri.Builder commonBuilder() {
        Uri.Builder builder = new Uri.Builder();

        builder.scheme("https")
                .authority(HOST)
                .appendPath(PATH_1);
        return builder;
    }

    // Get information on a specific team
    // https://www.thesportsdb.com/api/v1/json/1/searchteams.php?t=RC%20toulonnais
    private static final String ALL_ITEMS = "items";
    private static final String THUMBNAIL = "thumbnail";
    private static final String ALL = "ids";

    // Build URL to get information for a specific team
    public static URL buildSearchDevice(String deviceId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ALL_ITEMS).appendPath(deviceId);
        URL url = new URL(builder.build().toString());
        return url;
    }

    public static URL buildSearchDeviceThumbnail(String deviceId) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ALL_ITEMS).appendPath(deviceId).appendPath(THUMBNAIL);
        URL url = new URL(builder.build().toString());
        return url;
    }

    // Get last scores of a given team
    // https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=133604
    //private static final String TEAM_LAST_EVENT_PARAM1 = "id";
    public static URL buildSearchAllDevices() throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(ALL);
                //.appendQueryParameter(TEAM_LAST_EVENT_PARAM1, String.valueOf(idTeam));
        URL url = new URL(builder.build().toString());
        Log.d("url", builder.build().toString());
        return url;
    }

    // Get current team ranking in a championship for a given season
    /*private static final String CHAMPIONSHIP_TABLE = "lookuptable.php";
    private static final String CHAMPIONSHIP_TABLE_PARAM1 = "l";
    public static URL buildGetRanking(long idChampionship) throws MalformedURLException {
        Uri.Builder builder = commonBuilder();
        builder.appendPath(CHAMPIONSHIP_TABLE)
                .appendQueryParameter(CHAMPIONSHIP_TABLE_PARAM1, String.valueOf(idChampionship));
        URL url = new URL(builder.build().toString());
        return url;
    }*/

}
