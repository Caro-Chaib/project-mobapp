package com.example.projetl3;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import com.example.projetl3.data.Device;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.example.projetl3.data.Device.TAG;

public class ResultSearchActivity extends AppCompatActivity {

    public static RecyclerView recyclerView;
    public static ArrayList<Device> originalDevices;
    public static ArrayList<Device> usedDevices;
    public String research;
    public MainActivity ma;

    public static Drawable LoadImageFromWebOperations(URL url) {
        try {
            InputStream is = (InputStream) url.getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public static Drawable LoadImageFromWebOperations(String link) {
        try {
            URL url = new URL(link);
            InputStream is = (InputStream) url.getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_search);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        research = intent.getExtras().getString("caracteristics");

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        this.originalDevices = new ArrayList<Device>();
        this.usedDevices = new ArrayList<Device>();
        try {
            populate();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textView = (TextView)toolbar.findViewById(R.id.toolbarTextView);
        textView.setText("Le petit musée du CERI");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
        recyclerView.setItemViewCacheSize(0);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        ResultSearchActivity.IconicAdapter adapter = new ResultSearchActivity.IconicAdapter();
        recyclerView.setAdapter(adapter);
        sortOriginal();

        fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent switchToTeam = new Intent(view.getContext(), SearchDeviceActivity.class);
                SearchDeviceActivity.setMainActivity(new MainActivity());
                ResultSearchActivity.this.startActivity(switchToTeam);
            }
        });

    }

    public void populate() throws ExecutionException, InterruptedException {
        Log.d(TAG, "call populate");
        originalDevices = Device.getDevicesWithCaracteristics(MainActivity.originalDevices, research);
        sortOriginal();
    }

    public void addDevices(Device... devices) {
        for(Device d : devices) {
            this.originalDevices.add(d);
            Log.d(TAG,"add device " + d.getName() + "(" + d.getBrand() + ")" + "with cat " + d.dispCategories());
        }
    }

    public void sortOriginal() {
        this.usedDevices.removeAll(usedDevices);
        for(Device d : this.originalDevices) {
            this.usedDevices.add(d);
        }
    }

    public void sortAlpha() {
        this.usedDevices.removeAll(usedDevices);
        ArrayList<Device> dupDevices = originalDevices;
        for(Device d : Device.alphabSort(dupDevices)) {
            this.usedDevices.add(d);
        }
    }

    public void sortChrono() {
        this.usedDevices.removeAll(usedDevices);
        ArrayList<Device> dupDevices = originalDevices;
        for(Device d : Device.chronoSort(dupDevices)) {
            this.usedDevices.add(d);
        }
    }

    public void sortCat(String[] categories) {
        this.usedDevices.removeAll(usedDevices);
        ArrayList<Device> dupDevices = originalDevices;
        for(Device d : Device.catSort(dupDevices, categories)) {
            this.usedDevices.add(d);
        }
    }

    public Resources resources() {
        return this.getResources();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sortChrono) {
            recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            ResultSearchActivity.IconicAdapter adapter = new ResultSearchActivity.IconicAdapter();
            recyclerView.setAdapter(adapter);
            sortOriginal();
            sortChrono();
        }
        else if(id == R.id.action_sortAlpha) {
            recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            ResultSearchActivity.IconicAdapter adapter = new ResultSearchActivity.IconicAdapter();
            recyclerView.setAdapter(adapter);
            sortOriginal();
            sortAlpha();
        }
        else if(id == R.id.action_sortCat) {
            recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            ResultSearchActivity.IconicAdapter adapter = new ResultSearchActivity.IconicAdapter();
            recyclerView.setAdapter(adapter);
            sortOriginal();
            String[] categories = {"ordinateur de bureau", "ordinateur portable", "périphérique", "support de stockage", "ordinateur", "câble/adaptateur", "ordinateur de poche", "station de travail", "terminal de communication", "réseau", "Pas de catégories"};
            sortCat(categories);
        }
        else sortOriginal();

        return super.onOptionsItemSelected(item);
    }

    /*public void updateView() {
        IconicAdapter adapter = new IconicAdapter();
        recyclerView.setAdapter(adapter);
    }*/

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RowHolder rh = new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false), ResultSearchActivity.this);
            return rh;
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            final List devices = ResultSearchActivity.usedDevices;
            final Device current = ResultSearchActivity.usedDevices.get(position);
            holder.content = current;
            if(current != null) {
                try {
                    holder.bindModel(current);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getItemCount() {
            return(MainActivity.usedDevices.size());
        }
    }
}

