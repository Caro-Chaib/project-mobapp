package com.example.projetl3;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.projetl3.data.Device;

import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView name;
    ImageView icon;
    LinearLayout line;
    TextView cat;
    MainActivity main;
    ResultSearchActivity result;
    Device content;
    Drawable thumbnail;

    RowHolder(View row, MainActivity main) {
        super(row);
        this.name = (TextView) row.findViewById(R.id.name);
        this.cat = (TextView) row.findViewById(R.id.cat);
        this.icon = (ImageView) row.findViewById(R.id.image);
        this.line = (LinearLayout) row.findViewById(R.id.row);
        this.main = main;
        this.line.setOnClickListener(this);
    }

    RowHolder(View row, ResultSearchActivity main) {
        super(row);
        this.name = (TextView) row.findViewById(R.id.name);
        this.cat = (TextView) row.findViewById(R.id.cat);
        this.icon = (ImageView) row.findViewById(R.id.image);
        this.line = (LinearLayout) row.findViewById(R.id.row);
        this.result = main;
        this.line.setOnClickListener(this);
    }

    void bindModel(Device device) throws MalformedURLException {
        this.name.setText(device.toString());
        this.cat.setText(device.dispCategories());
        this.icon.setImageDrawable(device.getImage());
        /*if(device.getId() <= 9) {
            String id = Integer.toString((int) team.getId());
            int imgId = this.main.resources().getIdentifier("team" + id, "drawable", "com.example.tp3");
            this.icon.setImageResource(imgId);
        }*/
        //Log.d(SportDbHelper.class.getSimpleName(), "binded : "+team.getName()+" with league="+league.getText()+" and event="+event.getText().toString());
        this.content = device;
        this.thumbnail = device.getImage();
        //Drawable image = LoadImageFromWebOperations("https://www.thesportsdb.com/images/media/team/badge/90p5p01536392092.png");
        //this.icon.setImageResource(image);
        //Log.d(SportDbHelper.class.getSimpleName(), "image : "+image);
    }

    @Override
    public void onClick(View view) {
        int position = this.getAdapterPosition();
        Intent switchToDevice = new Intent(view.getContext(), DeviceActivity.class);
        switchToDevice.putExtra(Device.TAG, this.content);
        itemView.getContext().startActivity(switchToDevice);
    }

    public static Drawable LoadImageFromWebOperations(String url) {
        try {
            InputStream is = (InputStream) new URL(url).getContent();
            Drawable d = Drawable.createFromStream(is, url);
            return d;
        } catch (Exception e) {
            return null;
        }
    }

}
