package com.example.projetl3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SearchDeviceActivity extends AppCompatActivity {
    static MainActivity mainActivity;
    static EditText searchFields;

    public static void setMainActivity(MainActivity ma) {
        mainActivity = ma;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_device);

        searchFields = (EditText)findViewById(R.id.editSearch);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String fields = searchFields.getText().toString();
                Intent switchToList = new Intent(view.getContext(), ResultSearchActivity.class);
                switchToList.putExtra("caracteristics", fields);
                startActivity(switchToList);
            }
        });
    }
}
