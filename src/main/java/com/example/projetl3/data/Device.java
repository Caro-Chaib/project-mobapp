package com.example.projetl3.data;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.example.projetl3.MainActivity;

import java.util.ArrayList;

public class Device implements Parcelable {

    public static final String TAG = Device.class.getSimpleName();

    private int _id;
    private String id;
    private String name;
    private String brand;
    private int year;
    private String description;
    private String categoriesString;
    private ArrayList<String> categories;
    private int timeFrameBegin;
    private int timeFrameEnd;
    private boolean working;
    private Drawable thumbnail;
    private String thumbnailLink;
    private ArrayList<String> technicalDetails;
    private String technicalDetailsString;
    //private Image[] pictures (TODO : see how to manage that)

    public Device(int _id, String id) {
        this._id = _id;
        this.id = id;
        this.name = "";
        this.brand = "";
        this.year = 0;
        this.description = "";
        this.working = false;
        this.timeFrameBegin = 0;
        this.timeFrameEnd = 0;
        this.thumbnailLink = "";
        this.categories = new ArrayList<String>();
        this.technicalDetails = new ArrayList<String>();
        this.technicalDetailsString = "";
        this.categoriesString = "";
    }

    public Device(int _id, String id, String name, String brand, int year, String description, Integer frame) {
        this._id = _id;
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.year = year;
        this.description = description;
        this.working = false;
        this.timeFrameBegin = 0;
        this.timeFrameEnd = 0;
        this.timeFrameBegin = frame;
        this.thumbnailLink = "";
        this.categories = new ArrayList<String>();
        this.technicalDetails = new ArrayList<String>();
        this.technicalDetailsString = "";
        this.categoriesString = "";
    }

    public Device(int _id, String id, String name, String brand, int year, String description, boolean working, Integer frame) {
        this._id = _id;
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.year = year;
        this.description = description;
        this.working = working;
        this.timeFrameBegin = frame;
        this.timeFrameEnd = 0;
        this.thumbnailLink = "";
        this.categories = new ArrayList<String>();
        this.technicalDetails = new ArrayList<String>();
        this.technicalDetailsString = "";
        this.categoriesString = "";
    }

    // Constructor with time frame in two parts
    public Device(int _id, String id, String name, String brand, int year, String description, boolean working, Integer begin,  Integer end) {
        this._id = _id;
        this.id = id;
        this.name = name;
        this.brand = brand;
        this.year = year;
        this.description = description;
        this.working = working;
        this.timeFrameBegin = begin;
        this.timeFrameEnd = end;
        this.thumbnailLink = "";
        this.categories = new ArrayList<String>();
        this.technicalDetails = new ArrayList<String>();
        this.technicalDetailsString = "";
        this.categoriesString = "";
    }

    public ArrayList<String> getTechnicalDetails() {
        return technicalDetails;
    }

    public void addTechnicalDetails(String... details) {
        if(this.technicalDetailsString.equals("Pas de détails techniques")) this.categoriesString = "";
        for(String s : details) {
            this.technicalDetails.add(s);
            this.technicalDetailsString += ", "+s;
        }
    }

    public ArrayList<String> getCategories() {
        return categories;
    }

    public void addCategories(String... categories) {
        if(this.categoriesString.equals("Pas de catégories")) this.categoriesString = "";
        for(String s : categories) {
            this.categories.add(s);
            this.categoriesString += ", "+s;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    protected Device(Parcel in) {
        id = in.readString();
        name = in.readString();
        brand = in.readString();
        year = in.readInt();
        description = in.readString();
        Log.d(TAG, "parcel descrription : " + description);
        timeFrameBegin = in.readInt();
        timeFrameEnd = in.readInt();
        //working = in.readBoolean();
        categoriesString = in.readString();
        Log.d(TAG, "parcel categoriesString : " + categoriesString);
        technicalDetailsString = in.readString();
        Log.d(TAG, "parcel technicalDetailsString : " + technicalDetailsString);
        thumbnailLink = in.readString();
        thumbnail = MainActivity.LoadImageFromWebOperations(thumbnailLink);
        technicalDetails = new ArrayList<String>();
        categories = new ArrayList<String>();
        if(categoriesString == null) categoriesString = "Pas de catégories";
        if(technicalDetailsString == null) technicalDetailsString = "Pas de détails techniques";
    }

    public static final Creator<Device> CREATOR = new Creator<Device>() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public Device createFromParcel(Parcel in) {
            return new Device(in);
        }

        @Override
        public Device[] newArray(int size) {
            return new Device[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getYear() {
        return year;
    }

    public String yearToString() {
        if(year == 0) return "Date inconnue";
        else return Integer.toString(year);
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isWorking() {
        return working;
    }

    public void setAsWorking(boolean working) {
        this.working = working;
    }

    public int getFrameBegin() { return this.timeFrameBegin; }

    public void setFrameBegin(int begin) { this.timeFrameBegin = begin; }

    public int getFrameEnd() { return this.timeFrameEnd; }

    public void setFrameEnd(int end) { this.timeFrameEnd = end; }

    public String toString() {
        String res = name + " ";
        if(!brand.equals("")) res += "(" + brand + ") ";
        return res;
    }

    public String dispCategories() {
        //if(!categoriesString.equals("")) return categoriesString;
        if(this.categories.size() > 0) {
            String res = this.categories.get(0);
            for (String cat : this.categories) {
                if (!cat.equals(this.categories.get(0))) res += ", " + cat;
            }
            categoriesString = res;
            return res;
        }
        else if(!categoriesString.equals("")) return categoriesString;
        else return "Pas de catégories";
    }

    public String dispTechDetails() {
        //if(!technicalDetailsString.equals("")) return technicalDetailsString;
        if(this.technicalDetails.size() > 0) {
            String res = this.technicalDetails.get(0);
            for (String cat : this.technicalDetails) {
                if (!cat.equals(this.technicalDetails.get(0))) res += ", " + cat;
            }
            technicalDetailsString = res;
            return res;
        }
        else if(!technicalDetailsString.equals("")) return technicalDetailsString;
        else return "Pas de catégories";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(brand);
        dest.writeLong(year);
        dest.writeString(description);
        dest.writeInt(timeFrameBegin);
        dest.writeInt(timeFrameEnd);
        //dest.writeBoolean(working);
        dest.writeString(dispCategories());
        dest.writeString(dispTechDetails());
        dest.writeString(thumbnailLink);
    }

    public boolean isComplete() {
        return !(name.equals("") || brand.equals("") || year==0 || description.equals("") || timeFrameBegin==0 || categories.size()==0 || technicalDetails.size()==0);
    }

    public void setThumbnail(Drawable img, String url) {
        thumbnailLink = url;
        this.thumbnail = img;
    }

    public Drawable getImage() {
        if(thumbnail == null) return MainActivity.LoadImageFromWebOperations(thumbnailLink);
        return thumbnail;
    }

    public static ArrayList<Device> alphabSort(ArrayList<Device> devices) {
        ArrayList<Device> dupDevices = devices;
        ArrayList<Device> sortedDevices = new ArrayList<Device>();
        int i = devices.size()-1;
        while(i > 0) {
            Device first = dupDevices.get(i);
            int toRemove = i;
            for(int j=1 ; j<dupDevices.size() ; j++) {
                // If the result is positive, the argument precedes the object
                if (first.getName().compareToIgnoreCase(devices.get(j).getName()) > 0) {
                    first = dupDevices.get(j);
                    toRemove = j;
                }
            }
            sortedDevices.add(first);
            dupDevices.remove(toRemove);
            i--;
        }
        for(Device d : dupDevices) {
            sortedDevices.add(d);
        }
        return sortedDevices;
    }

    public static ArrayList<Device> chronoSort(ArrayList<Device> devices) {
        ArrayList<Device> dupDevices = devices;
        ArrayList<Device> sortedDevices = new ArrayList<Device>();
        int i = devices.size()-1;
        while(i > 0) {
            Device first = dupDevices.get(i);
            int toRemove = i;
            for(int j=0 ; j<dupDevices.size() ; j++) {
                // If the result is positive, the argument precedes the object
                if (first.getYear() >= dupDevices.get(j).getYear()) {
                    first = dupDevices.get(j);
                    toRemove = j;
                }
                /*else if(first.getYear() == dupDevices.get(j).getYear()) {
                    sortedDevices.add(first);
                    dupDevices.remove(toRemove);
                    first = dupDevices.get(j);
                    toRemove = j;
                    i--;
                }*/
            }
            sortedDevices.add(first);
            dupDevices.remove(toRemove);
            i--;
        }
        for(Device d : dupDevices) {
            sortedDevices.add(d);
        }
        return sortedDevices;
    }

    public static ArrayList<Device> catSort(ArrayList<Device> devices, String[] categories) {
        ArrayList<Device> dupDevices = devices;
        ArrayList<Device> sortedDevices = new ArrayList<Device>();
        for(String cat : categories) {
            for(Device device : dupDevices) {
                if(device.categories.contains(cat)) sortedDevices.add(device);
            }
        }
        return sortedDevices;
    }

    public static ArrayList<Device> getDevicesWithCaracteristics(ArrayList<Device> devices, String caracteristics) {
        ArrayList<Device> result = new ArrayList<Device>();
        for(Device d : devices) {
            boolean notThere = true;
            int i = 0;
            String current = d.getAllCaracteristics().get(i);
            while(notThere && i<d.getAllCaracteristics().size()) {
                if (caracteristics.contains(current)) {
                    notThere = false;
                    break;
                }
                current = d.getAllCaracteristics().get(i);
                i++;
            }
            if(!notThere) result.add(d);
        }
        return result;
    }

    public ArrayList<String> getAllCaracteristics() {
        ArrayList<String> caracteristics = new ArrayList<String>();
        caracteristics.add(id);
        caracteristics.add(name);
        caracteristics.add(brand);
        caracteristics.add(Integer.toString(year));
        caracteristics.add(description);
        caracteristics.addAll(technicalDetails);
        caracteristics.addAll(categories);
        return caracteristics;
    }
}
