package com.example.projetl3;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;

import com.example.projetl3.data.Device;
import com.example.projetl3.webservice.JSONResponseHandlerDevice;
import com.example.projetl3.webservice.JSONResponseHandlerIds;
import com.example.projetl3.webservice.WebServiceUrl;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static com.example.projetl3.data.Device.TAG;

public class MainActivity extends AppCompatActivity {

    public static RecyclerView recyclerView;
    public static ArrayList<Device> originalDevices;
    public static ArrayList<Device> usedDevices;

    public static Drawable LoadImageFromWebOperations(URL url) {
        try {
            InputStream is = (InputStream) url.getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    public static Drawable LoadImageFromWebOperations(String link) {
        try {
            URL url = new URL(link);
            InputStream is = (InputStream) url.getContent();
            Drawable d = Drawable.createFromStream(is, "src name");
            return d;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //this.dbHelper = new DeviceDbHelper(this);
        this.originalDevices = new ArrayList<Device>();
        this.usedDevices = new ArrayList<Device>();
        try {
            populate();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView textView = (TextView)toolbar.findViewById(R.id.toolbarTextView);
        textView.setText("Le petit musée du CERI");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
        recyclerView.setItemViewCacheSize(0);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        IconicAdapter adapter = new IconicAdapter();
        recyclerView.setAdapter(adapter);
        sortOriginal();

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent switchToTeam = new Intent(view.getContext(), SearchDeviceActivity.class);
                SearchDeviceActivity.setMainActivity(MainActivity.this);
                MainActivity.this.startActivity(switchToTeam);
            }
        });

    }

    public void populate() throws ExecutionException, InterruptedException {
        Log.d(TAG, "call populate");
        //String[] ids = {"tsx","9lr","tnx","hsv","q2u","kyt","rkj","g1c","yzw","w1s","4ar","lf8","g01","jne","4w6","r6j","flh","cza","lon","rxs","0qf","ave","gex","hjy","mfj","c1z","ci6","xnw","9x3","4zh","80t","jxv","k0j","ry8","cdn","51a","8xi","ace","blm","jjh","2j7"};
        MyAllAsyncTask2 getIds = new MyAllAsyncTask2(MainActivity.this);
        ArrayList<String> ids = getIds.execute().get();
        MyAllAsyncTask async = new MyAllAsyncTask(MainActivity.this, ids);
        originalDevices = async.execute().get();
        Log.d(TAG,"execute async");
    }

    public void addDevices(Device... devices) {
        for(Device d : devices) {
            this.originalDevices.add(d);
            Log.d(TAG,"add device " + d.getName() + "(" + d.getBrand() + ")" + "with cat " + d.dispCategories());
        }
    }

    public void sortOriginal() {
        this.usedDevices.removeAll(usedDevices);
        for(Device d : this.originalDevices) {
            this.usedDevices.add(d);
        }
    }

    public void sortAlpha() {
        this.usedDevices.removeAll(usedDevices);
        ArrayList<Device> dupDevices = originalDevices;
        for(Device d : Device.alphabSort(dupDevices)) {
            this.usedDevices.add(d);
        }
    }

    public void sortChrono() {
        this.usedDevices.removeAll(usedDevices);
        ArrayList<Device> dupDevices = originalDevices;
        for(Device d : Device.chronoSort(dupDevices)) {
            this.usedDevices.add(d);
        }
    }

    public void sortCat(String[] categories) {
        this.usedDevices.removeAll(usedDevices);
        ArrayList<Device> dupDevices = originalDevices;
        for(Device d : Device.catSort(dupDevices, categories)) {
            this.usedDevices.add(d);
        }
    }

    public Resources resources() {
        return this.getResources();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_sortChrono) {
            recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            IconicAdapter adapter = new IconicAdapter();
            recyclerView.setAdapter(adapter);
            sortOriginal();
            sortChrono();
        }
        else if(id == R.id.action_sortAlpha) {
            recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            IconicAdapter adapter = new IconicAdapter();
            recyclerView.setAdapter(adapter);
            sortOriginal();
            sortAlpha();
        }
        else if(id == R.id.action_sortCat) {
            recyclerView = (RecyclerView) findViewById(R.id.recyclerViewDevices);
            recyclerView.setItemViewCacheSize(0);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
            IconicAdapter adapter = new IconicAdapter();
            recyclerView.setAdapter(adapter);
            sortOriginal();
            String[] categories = {"ordinateur de bureau", "ordinateur portable", "périphérique", "support de stockage", "ordinateur", "câble/adaptateur", "ordinateur de poche", "station de travail", "terminal de communication", "réseau", "Pas de catégories"};
            sortCat(categories);
        }
        else sortOriginal();

        return super.onOptionsItemSelected(item);
    }

    /*public void updateView() {
        IconicAdapter adapter = new IconicAdapter();
        recyclerView.setAdapter(adapter);
    }*/

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            RowHolder rh = new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false), MainActivity.this);
            return rh;
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            final List devices = MainActivity.usedDevices;
            final Device current = MainActivity.usedDevices.get(position);
            holder.content = current;
            if(current != null) {
                try {
                    holder.bindModel(current);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            }
        }

        @Override
        public int getItemCount() {
            return(MainActivity.usedDevices.size());
        }
    }
}

class MyAllAsyncTask extends AsyncTask<Object, Void, ArrayList<Device>> {
    private MainActivity activity;
    private ArrayList<String> ids;
    private ArrayList<Device> jsonDevices;

    // only retain a weak reference to the activity
    MyAllAsyncTask(MainActivity context) {
        activity = context;
        jsonDevices = new ArrayList<Device>();
    }

    MyAllAsyncTask(MainActivity context, ArrayList<String> ids) {
        activity = context;
        this.ids = ids;
        jsonDevices = new ArrayList<Device>();
    }

    @Override
    protected ArrayList<Device> doInBackground(Object[] objects) {
        Log.d(TAG,"do in background ");
        int i = 0;
        for (String id : ids) {
            try {
                Device device = new Device(i, id);
                URL deviceUrl = null;
                HttpURLConnection urlConnectionDevice = null;
                URL thumbnailUrl = null;
                HttpURLConnection urlConnectionThumbnail = null;

                deviceUrl = WebServiceUrl.buildSearchDevice(id);
                urlConnectionDevice = (HttpURLConnection) deviceUrl.openConnection();
                InputStream inDevice = new BufferedInputStream(urlConnectionDevice.getInputStream());
                JSONResponseHandlerDevice jsonDevice = new JSONResponseHandlerDevice(device);
                jsonDevice.readJsonStream(inDevice);

                thumbnailUrl = WebServiceUrl.buildSearchDeviceThumbnail(id);
                Drawable d = MainActivity.LoadImageFromWebOperations(thumbnailUrl);
                device.setThumbnail(d, thumbnailUrl.toString());
                /*urlConnectionThumbnail = (HttpURLConnection) thumbnailUrl.openConnection();
                InputStream inPic = new BufferedInputStream(urlConnectionThumbnail.getInputStream());
                JSONResponseHandlerThumbnail jsonPic = new JSONResponseHandlerThumbnail(device);
                jsonPic.readJsonStream(inPic);*/

                jsonDevices.add(device);
                Log.d(TAG, "Updating : " + id + "(" + jsonDevices.size() + "th element)" + " with brand " + device.getBrand());
                i++;

            } catch (IOException e) {
                Log.d(TAG, "Error while updating : " + id);
                e.printStackTrace();
            }
        }
        return jsonDevices;
    }

    @Override
    public void onPostExecute(ArrayList<Device> param) {
        super.onPostExecute(param);
    }
}

class MyAllAsyncTask2 extends AsyncTask<Object, Void, ArrayList<String>> {
    private MainActivity activity;
    private ArrayList<String> ids;

    // only retain a weak reference to the activity
    MyAllAsyncTask2(MainActivity context) {
        activity = context;
        ids = new ArrayList<String>();
    }

    @Override
    protected ArrayList<String> doInBackground(Object[] objects) {
        Log.d(TAG, "do in background ");
        URL devicesUrl = null;
        HttpURLConnection urlConnectionDevices = null;
        try {
            devicesUrl = WebServiceUrl.buildSearchAllDevices();
            urlConnectionDevices = (HttpURLConnection) devicesUrl.openConnection();
            InputStream inDevice = null;
            inDevice = new BufferedInputStream(urlConnectionDevices.getInputStream());
            JSONResponseHandlerIds jsonDevice = new JSONResponseHandlerIds(ids);
            jsonDevice.readJsonStream(inDevice);
        } catch (IOException e) {
            Log.d(TAG, "Error while getting all ids");
            e.printStackTrace();
        }
        return ids;
    }

    @Override
    public void onPostExecute(ArrayList<String> param) {
        super.onPostExecute(param);
    }
}