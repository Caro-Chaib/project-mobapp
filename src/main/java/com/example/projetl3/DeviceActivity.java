package com.example.projetl3;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetl3.data.Device;
import com.example.projetl3.webservice.JSONResponseHandlerDevice;
import com.example.projetl3.webservice.WebServiceUrl;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import static com.example.projetl3.data.Device.TAG;

public class DeviceActivity extends AppCompatActivity {

    private Device device;
    private TextView textName;
    private TextView textBrand;
    private ImageView imageView;
    private TextView textYear;
    private TextView textCategories;
    private TextView textTechDetails;
    private TextView textDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);

        this.device = (Device) getIntent().getParcelableExtra(Device.TAG);

        MyAllAsyncTask3 getDrawable = new MyAllAsyncTask3(DeviceActivity.this, device);
        Drawable drawable = null;
        Device dup = null;
        try {
            dup = getDrawable.execute().get();
            drawable = device.getImage();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

        textName = (TextView) findViewById(R.id.name);
        if(!device.getBrand().equals("")) textName.setText(device.getName() + " (" + device.getBrand() + ")");
        else textName.setText(device.getName());

        textYear = (TextView) findViewById(R.id.editYear);
        textYear.setText(device.yearToString());

        textCategories = (TextView) findViewById(R.id.editCategories);
        textCategories.setText(dup.dispCategories());

        textTechDetails = (TextView) findViewById(R.id.editTechDetails);
        textTechDetails.setText(dup.dispTechDetails());

        textDescription = (TextView) findViewById(R.id.description);
        Log.d(TAG, "description : " + dup.getDescription());
        textDescription.setText(dup.getDescription() + "\n \n");

        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setImageDrawable(drawable);

    }
}

class MyAllAsyncTask3 extends AsyncTask<Object, Void, Device> {
    private DeviceActivity activity;
    private Device content;

    MyAllAsyncTask3(DeviceActivity context, Device device) {
        activity = context;
        content = device;
    }

    @Override
    protected Device doInBackground(Object[] objects) {
        Log.d(TAG,"do in background ");
        Drawable drawable;
        try {
            String id = content.getId();
            URL deviceUrl = null;
            HttpURLConnection urlConnectionDevice = null;
            URL thumbnailUrl = null;
            HttpURLConnection urlConnectionThumbnail = null;

            thumbnailUrl = WebServiceUrl.buildSearchDeviceThumbnail(id);
            drawable = MainActivity.LoadImageFromWebOperations(thumbnailUrl);
            content.setThumbnail(drawable, thumbnailUrl.toString());
            deviceUrl = WebServiceUrl.buildSearchDevice(id);
            urlConnectionDevice = (HttpURLConnection) deviceUrl.openConnection();
            InputStream inDevice = new BufferedInputStream(urlConnectionDevice.getInputStream());
            JSONResponseHandlerDevice jsonDevice = new JSONResponseHandlerDevice(content);
            jsonDevice.readJsonStream(inDevice);
            return content;

        } catch (IOException e) {
            Log.d(TAG, "Error while updating : " + content.getId());
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onPostExecute(Device param) {
        super.onPostExecute(param);
    }
}
